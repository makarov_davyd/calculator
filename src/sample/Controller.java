package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

public class Controller{

    @FXML
    private TextField money;

    @FXML
    private Button button;

    @FXML
    private Label chai;

    @FXML
    private Label val;


    @FXML
    private MenuButton valute;

    @FXML
    private MenuItem rub;

    @FXML
    private MenuItem euro;

    @FXML
    private MenuItem shek;

    @FXML
    private MenuItem grivn;

    @FXML
    private MenuItem dollar;

    @FXML
    private RadioButton radioZero;

    @FXML
    private ToggleGroup RadioButton;

    @FXML
    private RadioButton radioThree;

    @FXML
    private RadioButton radioFive;

    @FXML
    private RadioButton radioSeven;

    @FXML
    private RadioButton radioTen;
    private int procent;
    private int curse;
    private String nac;



    @FXML
    void actDollar(ActionEvent event) {
        valute.setText("Доллары");


    }

    @FXML
    void actEuro(ActionEvent event) {
        valute.setText("Евро");

    }

    @FXML
    void actGrivn(ActionEvent event) {
        valute.setText("Гривны");

    }

    @FXML
    void actRub(ActionEvent event) {
        valute.setText("Рубли");
        nac = "Рубли";

    }

    @FXML
    void actShek(ActionEvent event) {
        valute.setText("Шекели");

    }

    @FXML
    void fiveProcent(ActionEvent event) {
        procent = 5;

    }

    @FXML
    void sellButton(ActionEvent event, String nac, String money2) {
        if (nac == "Рубли"){
            curse = Integer.parseInt(money.getText());
            chai.setText(String.valueOf(curse * 5));

        }

    }

    @FXML
    void sevenProcent(ActionEvent event) {
        procent = 7;

    }

    @FXML
    void tenProcent(ActionEvent event) {
        procent = 10;

    }

    @FXML
    void threeProcent(ActionEvent event) {
        procent = 3;

    }

    @FXML
    void zeroProcent(ActionEvent event) {
        procent = 0;

    }

    void setGroup(RadioButton radioButton){
        final ToggleGroup group = new ToggleGroup();
        RadioButton radioZero = new RadioButton("Z");
        radioZero.setToggleGroup(group);
        radioZero.setSelected(true);
        RadioButton radioThree = new RadioButton("th");
        radioThree.setToggleGroup(group);
        RadioButton radioFive = new RadioButton("f");
        radioFive.setToggleGroup(group);
        RadioButton radioSeven = new RadioButton("s");
        radioSeven.setToggleGroup(group);
        RadioButton radioTen = new RadioButton("ten");
        radioTen.setToggleGroup(group);
    }

}
